// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

const {ipcRenderer} = require('electron');

const com_list = document.getElementById('com_list');
const btn = document.getElementById('btn');
const tip_list = document.getElementById('tip')
let has_com = false
let com = null

function config() {
    if (has_com) {
        let index = com_list.selectedIndex;

        com = com_list.options[index].text

        btn.setAttribute('disabled', 'true')
        btn.innerText = '请稍等...'
        btn.style.backgroundColor = '#F40'

        tip_list.innerHTML = '<li>连接串口：' + com + '</li>'

        ipcRenderer.send('config', {
            port: com
        })
    } else {
        btn.innerText = '请先选择正确串口'

        setTimeout(function () {
            btn.innerText = '开始配置'
        }, 1000)
    }
}

function refresh() {
    ipcRenderer.send('refresh')
}

ipcRenderer.on('com_list', (e, msg) => {
    if (msg.com !== undefined) {
        let select = ''
        for (let i = 0; i < msg.com.length; i++) {
            select += '<option>' + msg.com[i].path + '</option>'
        }
        com_list.innerHTML = select

        has_com = true
    } else {
        has_com = false
    }
})

ipcRenderer.on('error', (e, msg) => {
    switch (msg.step) {
        case 0:
            tip_list.innerHTML = '<li>连接串口：' + com + '……失败</li>'
            break;
        case 1:
            tip_list.innerHTML += '<li>灯光设置……失败</li>'
            break;
        case 2:
            tip_list.innerHTML += '<li>禁用休眠……失败</li>'
            break;
        case 3:
            tip_list.innerHTML += '<li>修改蜂鸣频率……失败</li>'
            break;
        case 4:
            tip_list.innerHTML += '<li>修改蜂鸣时长……失败</li>'
            break;
        case 5:
            tip_list.innerHTML += '<li>开启提示音……失败</li>'
            break;
        case 6:
            tip_list.innerHTML += '<li>GBK编码、串口输出……失败</li>'
            break;
        case 7:
            tip_list.innerHTML += '<li>关闭旋转、禁用所有条码……失败</li>'
            break;
        case 8:
            tip_list.innerHTML += '<li>启用Code128……失败</li>'
            break;
        case 9:
            tip_list.innerHTML += '<li>按键触发……失败</li>'
            break;
        case 10:
            tip_list.innerHTML += '<li>保存至EEPROM……失败</li>'
            break;
        default:
            tip_list.innerHTML += '<li>未知操作……失败</li>'
            break;
    }

    btn.innerText = '配置失败'
    btn.style.backgroundColor = 'red'

    setTimeout(function () {
        btn.removeAttribute('disabled')
        btn.innerText = '开始配置'
        btn.style.backgroundColor = 'blue'
    }, 1000)
})

ipcRenderer.on('success', (e, msg) => {
    switch (msg.step) {
        case 0:
            tip_list.innerHTML = '<li>连接串口：' + com + '……成功</li>'
            break;
        case 1:
            tip_list.innerHTML += '<li>灯光设置……成功</li>'
            break;
        case 2:
            tip_list.innerHTML += '<li>禁用休眠……成功</li>'
            break;
        case 3:
            tip_list.innerHTML += '<li>修改蜂鸣频率……成功</li>'
            break;
        case 4:
            tip_list.innerHTML += '<li>修改蜂鸣时长……成功</li>'
            break;
        case 5:
            tip_list.innerHTML += '<li>开启提示音……成功</li>'
            break;
        case 6:
            tip_list.innerHTML += '<li>GBK编码、串口输出……成功</li>'
            break;
        case 7:
            tip_list.innerHTML += '<li>关闭旋转、禁用所有条码……成功</li>'
            break;
        case 8:
            tip_list.innerHTML += '<li>启用Code128……成功</li>'
            break;
        case 9:
            tip_list.innerHTML += '<li>按键触发……成功</li>'
            break;
        case 10:
            tip_list.innerHTML += '<li>保存至EEPROM……成功</li>'
            break;
        default:
            tip_list.innerHTML += '<li>未知操作……成功</li>'
            break;
    }

    if (msg.step === 9) {
        btn.innerText = '配置成功'
        btn.style.backgroundColor = 'green'

        setTimeout(function () {
            btn.removeAttribute('disabled')
            btn.innerText = '开始配置'
            btn.style.backgroundColor = 'blue'
        }, 1000)
    }
})

refresh()
